package org.ual.es.ejercicio.refactorizacionindividual;

import java.util.ArrayList;
import java.util.Iterator;

public class Customer {

	private String name;
	private ArrayList rentals = new ArrayList();

	public Customer(String strName) {
		this.name = strName;
	}

	public void addRental(Rental arg) {
		rentals.add(arg);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;

		Iterator<Rental> itrentals = rentals.iterator();

		String result = "Rental Record for " + getName() + "\n";

		while (itrentals.hasNext()) {
			Rental each = itrentals.next();
			frequentRenterPoints += each.getFrequentRenterPoints();

			result += "\t" + each.getMovie().getTitle() + " " + each.getCharge() + "\n";
			totalAmount += each.getCharge();
		}

		result += "Amount owed is " + totalAmount + "\n";
		result += "You earned " + frequentRenterPoints + " frequent renter points";
		return result;
	}
}